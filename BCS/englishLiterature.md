# English: Literature
  - [1. আলাউদ্দিন আল আজাদ](#আলাউদ্দিন-আল-আজাদ)
  - [2. আখতারুজ্জামান ইলিয়াস](#আখতারুজ্জামান-ইলিয়াস)

<!-- bangladeshi writer all -->
[bangladeshi writer](https://bn.wikipedia.org/wiki/বিষয়শ্রেণী:বাঙালি_ঔপন্যাসিক)

<a name="custom_back"></a>
## Important Period of English Literature: 
<table style="width:100%">
    <tr>
        <th>English-Period</th>
        <th>Sub-Period</th>
        <th>Description</th>
        <th>Writer</th>
    </tr>
    <tr>
        <td rowspan="3"> 
            Middle English Period <strong>1066-1500</strong>
        </td>
        <td>
            Anglo Norman Period <strong> 1066-1340</strong>
        </td>
        <td></td>
        <td rowspan="3">
            <ol>
               <a href="#Geoffrey_Chaucer"> <li>Geoffrey Chaucer</li> </a>
                <li>John Wycliff </li>
            </ol>
        </td>
    </tr>
    <tr>
        <td>
            The Age of Chaucer <strong> 1340-1400</strong>     
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            The Dark/Barren Period <strong> 1400-1485/1500</strong>   
        </td>
        <td>
        </td>
    </tr>
    <!-- end of first raw -->
    <!-- staring the renaissance period -->
    <tr>
        <td rowspan="5"> The Renaissance: <strong>1500-1660</strong> </td>
        <td>Preparation For Renaissance <strong>1500-1558</strong> </td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>The Elizabethan Period <strong>1558-1603</strong> </td>
        <td></td>
        <td></td>
    </tr>
     <tr>
        <td>The Jacobean Period <strong>1603-1625</strong> </td>
        <td></td>
        <td></td>
    </tr>
     <tr>
        <td>The Caroline Period <strong>1625-1649</strong> </td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>The Commonwealth Period <strong>1649-1660</strong> </td>
        <td></td>
        <td></td>
    </tr>
    <!-- end of ranaissance period -->
    <!-- Start of Neo Classical period -->
    <tr>
        <td rowspan="3"> The Neo-Classical Period <strong>1660-1798</strong></td>
        <td>The Restoration Period <strong>1660-1700</strong> </td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>The Augustan Period <strong>1700-1745</strong> </td>
        <td>
            <ul>
               <li>Alexander Pope</li>
            </ul>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>The Age of Sensibility<strong>1745-1785/1798</strong> </td>
        <td>
            <ul>
                <li>The age of Reason/Transition</li>
                <li>Dr. Samuel Jhonson</li>
            </ul>
        </td>
        <td></td>
    </tr>
    <!-- end of neo classicial period -->
    <!-- Start of Romantic period -->
     <tr>
        <td>The Romantic period <strong>1798-1832</strong> </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <!-- end of roamntic period -->
    <!-- start of victorian period -->
    <tr>
        <td rowspan="2"> The Victorian Period <strong>1832-1901</strong> </td>
        <td>The Pre-Raphaelites <strong>1848-1860</strong> </td>
        <td></td>
        <td rowspan="2">
            <ol> 
                <a href="#Lord_Alfred_Tennyson"> <li>Lord Alfred Tennyson</li> </a>
                <a href="#Robert_Browning"> <li>Robert Browning</li> </a>
            </ol>
        </td>
    </tr>
    <tr>
        <td>Aestheticism and Decadence <strong> 1880-1901 </strong> </td>
        <td></td>
    </tr>
    <!-- end of victorian period -->
    <!-- start of modan period -->
    <tr>
        <td rowspan="2">The Modan Period <strong>1901-1939</strong> </td>
        <td>The Edwardian period <strong>1901-1910</strong> </td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>The Georian Period <strong>1910-1936</strong> </td>
        <td></td>
        <td></td>
    </tr>
    <!-- end of modan periond -->
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</table>

<a name="Geoffrey_Chaucer"></a> 
## Geoffrey Chaucer  <a href="#custom_back">Back</a>
<!--  start Lord_Alfred_Tennyson -->
<a name="Lord_Alfred_Tennyson"></a>
## Lord Alfred Tennyson <a href="#custom_back">Back</a>

[Lord Alfred Tennyson](https://en.wikipedia.org/wiki/Alfred,_Lord_Tennyson) 1st Baron Tennyson FRS (6 August 1809 – 6 October 1892) was a British poet. 

<table style="width:100%">
  <tr>
    <th>Famous Poems</th>
    <th>Famous Comedies</th>
    <th>Famous Elegy </th>
    <th>Quote</th>
  </tr> 
  <tr>
    <td>
      <ul>
        <li>Oneone</li>
        <li>Ulysses</li>
        <li>lotus Eaters</li>
        <li>The Lady of Shalott</li>
        <li>Locksley</li>
        <li>Tears Idle Tears</li>
        <li>Tithonus</li>
        <li>the Two vocies</li>
        <li>The Charge of the light Bridge</li>
        <li>morthe D' Arthur</li>
        <li></li>
      </ul>
    </td>
    <td>
      <ul>
        <li></li>
         <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
      </ul>
    </td>
    <td>
      <ul>
        <li></li>
         <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>The old order changeth yeilding place to new</li>
         <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
      </ul>
    </td>
  </tr>
</table> 
<!-- end of Lord_Alfred_Tennyson -->
<!-- start Robert_Browning -->
<a name="Robert_Browning"></a>
## Robert Browning  <a href="#custom_back">Back</a>

[Robert Browning](https://en.wikipedia.org/wiki/Robert_Browning)
 (7 May 1812 – 12 December 1889) was an English poet and playwright whose dramatic monologues put him among the foremost Victorian poets. His poems are noted for irony, characterization, dark humour, social commentary, historical settings and challenging vocabulary and syntax.
<table style="width:100%">
  <tr>
    <th>Bookes of Poems</th>
    <th>Famous Poewms</th>
    <th>Quotes of Robert Browing</th>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Men and Women</li>
        <li>Dramatic Lyrics</li>
        <li>The Ring of the Book</li>
      </ul>
    </td>
    <td>
      <ul>
        <li>My Last Duchess</li>
        <li>Andrea Dei Sarto</li>
        <li>Porphyria's Lover</li>
        <li>Rabbi Ben Ezra</li>
        <li>Fra lippo Lippi</li>
        <li>The Patriot <strong>(Patriotism by Sir Water Scout)</strong> </li>
        <li>The Ring and the Book</li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Thus I enter and thus I go!</li>
      </ul>
    </td>
  </tr>
</table> 
<!-- end og Robert_Browning -->

<!-- common formate -->

<a name="Robert_Browning"></a>
## common <a href="#custom_back">Back</a>

[common](https://en.wikipedia.org/wiki/common)

<table style="width:100%">
  <tr>
    <th>Bookes of Poems</th>
    <th>Famous Poewms</th>
    <th>Quotes of Robert Browing</th>
  </tr>
  <tr>
    <td>
      <ul>
        <li></li>
        <li></li>
        <li></li>
      </ul>
    </td>
    <td>
      <ul>
        <li></li>
      </ul>
    </td>
    <td>
      <ul>
        <li></li>
      </ul>
    </td>
  </tr>
</table> 
<!-- end og Robert_Browning -->
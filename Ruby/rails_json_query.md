```ruby
http://stackoverflow.com/questions/22667401/postgres-json-data-type-rails-query
http://stackoverflow.com/questions/40702813/query-on-postgres-json-array-field-in-rails

#payload: [{"kind"=>"person"}]
Model_Name.where("payload @> ?", [{kind: "person"}].to_json)

#data: {"interest"=>["music", "movies", "programming"]}
Model_Name.where("data @> ?",  {"interest": ["music", "movies", "programming"]}.to_json)
Model_Name.where("data #>> '{interest, 1}' = 'movies' ")
Model_Name.where("jsonb_array_length(data->'interest') > 1")
Model_Name.where("data->'interest' ? :value", value: "movies") 
Model_Name.where("data -> 'interest' ? :value", value: ['programming'])

data: {"customers"=>[{:name=>"david"}]}
Model_Name.where("data #> '{customers,0}' ->> 'name' = 'david' ")
Model_Name.where("data @> ?",  {"customers": [{"name": "david"}]}.to_json)
Model_Name.where("data -> 'customers' @> '[{\"name\": \"david\"}]'")
Model_Name.where(" data -> 'customers' @> ?", [{name: "david"}].to_json)

#data: [ {"name":"hafizul-1", "age"=12}, {"name":"hafizul-2", "age"=13}, {"name":"hafizul-3", "age"=24}]

Model_Name.where('data @> ?', '[{"age": 12}]')

#data: {"uid"=>"5", "blog"=>"recode"}
Model_Name.where("data @> ?", {uid: '5'}.to_json)
Model_Name.where("data ->> 'blog' = 'recode'")
Model_Name.where("data ->> 'blog' = ?", "recode")
Model_Name.where("data ? :key", :key => 'uid')
Model_Name.where("data -> :key LIKE :value",     :key => 'blog', :value => "%recode%")

#data: 

#tags: ["dele, jones", "solomon"]
# get a single tag
#Model_Name.where("'solomon' = ANY (tags)")
# which segments are tagged with 'solomon'
Model_Name.where('? = ANY (tags)', 'solomon')
# which segments are not tagged with 'solomon'
Model_Name.where('? != ALL (tags)', 'solomon')
# or
Model_Name.where('NOT (? = ANY (tags))', 'solomon')
#multiple tags
Model_Name.where("tags @> ARRAY[?]::varchar[]", ["dele, jones", "solomon"])
# tags with 3 items
Model_Name.where("array_length(tags, 1) >= 3")

# SUM (Thanks @skplunkerin)
https://gist.github.com/mankind/1802dbb64fc24be33d434d593afd6221#gistcomment-2711098
`https://stackoverflow.com/a/39280048/1180523`

#data: [{"amount"=>12.0},{"amount"=>25.50},{"amount"=>17.99}]
Model_Name.select("SUM((data ->> 'amount')::FLOAT) AS total_amount")
```

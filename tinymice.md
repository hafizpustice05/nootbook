```html
<body>
    <textarea class="textarea" name="description" id="description" placeholder="Product Description"
        rows="5"></textarea>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
    <script
        src="http://cloud.tinymce.com/stable/tinymce.min.js?apiKey=mwfymtkmnxvn29ty7yuwclaktrwb88yos8ykan12ot9yhz1f">
    </script>
    <script>
        tinymce.init( {
            selector: '.textarea',
            plugins: 'advlist autolink lists textcolor colorpicker link code wordcount media image imagetools searchreplace charmap anchor visualblocks fullscreen table contextmenu paste',
            menubar: true,
            height: "250",
            toolbar1: 'insertfile | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor link code media image',
            rel_list: [ {
                title: 'nofollow',
                value: 'nofollow'
            }, {
                title: 'follow',
                value: 'follow'
            } ],
            toolbar: "...| removeformat | ...",
            media_live_embeds: true,
            relative_urls: false,
            remove_script_host: false,
            video_template_callback: function ( data ) {
                return '<video width="' + data.width + '" height="' + data.height + '"' + ( data.poster ?
                        ' poster="' + data.poster + '"' : '' ) + ' controls="controls">\n' +
                    '<source src="' + data.source1 + '"' + ( data.source1mime ? ' type="' + data
                        .source1mime + '"' : '' ) + ' />\n' + ( data.source2 ? '<source src="' + data
                        .source2 + '"' + ( data.source2mime ? ' type="' + data.source2mime + '"' : '' ) +
                        ' />\n' : '' ) + '</video>';
            },
            extended_valid_elements: 'script[type|src|charset]',
          setup: function(ed) {
                                ed.on('change', function(e) {
                                    tinyMCE.triggerSave();
                                });
                            }
        } );

    </script>
</body>

```

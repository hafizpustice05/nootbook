
# pull the base image
FROM node:17.9.0
# set the working direction
WORKDIR /code
ENV PATH /code/node_modules/.bin:$PATH

# install app dependencies
COPY package.json /code/package.json

COPY yarn.lock /code/yarn.lock


RUN yarn

# add app
COPY . /code
EXPOSE 3001
# start app
CMD ["yarn", "start"]
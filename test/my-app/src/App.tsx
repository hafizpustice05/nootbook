import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios'




const api =axios.create({
baseURL: 'http://127.0.0.1:3000/api/v1/survey_api_provider'
})


class App extends Component {
  state = {
    providers: []
  }
  constructor(){
    super({},{});
    this.getProviders();
  }
  getProviders = async()=>{
    let responses = await api.get('/').then(({data})=>data.data);
    console.log('data: ',responses)
    this.setState({providers: responses})
  }
  

  render(){
  return (
    <div className="App">
      {this.state.providers.map(provider => <h2>
        {provider.name}
      </h2>)}
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload. Hello Hafiz
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
  }
}

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.tsx</code> and save to reload. Hello Hafiz
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

export default App;

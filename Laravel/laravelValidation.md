```php
public function rules()
    {
        return [
            'refered_user_id' => 'required',
            'relation' => 'nullable|integer',
            'email' => "nullable|string|email|max:55|unique:users,email,{$id},id",
            'country_code' => 'required|regex:/(880)/',
            'phone_number' => 'required|regex:/(1)[0-9]{9}/|exists:temp_registration_otps,phone_number',
            'name' => 'required',
            'mobileNumber' => 'required|regex:/(1)[0-9]{9}/|unique:users,phone_number',
            'email' => 'nullable|email',
            'dateOfBirth' => 'nullable|date',
            'gender' => 'nullable',
            'password' => 'required|string||min:6|required_with:confirmPassword|same:confirmPassword',
            'confirmPassword' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'mobileNumber' => 'Please give a valid number.',
            'mobileNumber.regex' => 'Please give a valid number.',
            'mobileNumber.unique' => 'The number is already taken.',
            'email' => 'Email must be a valid email',
            'password.required' => 'Password must be atleast 6 characters',
        ];
    }
    protected function prepareForValidation()
    {
        $userId = auth()->id();
        $this->merge(['refered_user_id' => $userId]);

        if ($this->has('mobileNumber')) {
            $this->merge(['mobileNumber' => (int) $this->mobileNumber]);
        }
    }


```

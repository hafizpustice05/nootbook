```PHP
 public function getLoginActivity(Request $request)
    {
        $search = $request->search;
        ;
        return Activity::with(['user.organization'])->whereHas('user.organization',function($q)use($search){
            return $q->where('name','ilike',"%$search%");
        })
        ->orWhereHas('user',function($q)use($search){
           return $q->where('name','ilike',"%$search%");            
         })
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        $search = $request->search;
        return Activity::with(['user.organization'=>function($q)use($search){
            return $q->when($search,function($qq)use($search){
                $qq->where('user.organization.name',$search)
                ->where('user.name',$search);
            });

        }])
            ->orderBy('created_at', 'desc')
            ->paginate(10);
    }
```

#Laravel Eloquent search customize
```PHP

$bangladeshUsers = User::where('role', 3)->with('profile', 'profile.occupation', 'profile.specialization')
            ->whereHas('profile', function ($profile) use ($bloodGroup) {
                return $profile->whereNotNull('blood_group')
                    ->where('is_blood_donor', 1)
                    ->where('country', '=', 19)
                    ->when(\request('blood_group'), function ($profile) use ($bloodGroup) {
                        return $profile->where('blood_group', '=', $bloodGroup);
                    });
            })
            ->when(\request('intake_no'), function ($q) use ($request) {
                return $q->where('intake_no', '=', $request->intake_no);
            })
            ->select('*', DB::raw('1 AS sort_order'), DB::raw("CAST(cadet_no AS UNSIGNED) as CADET_SORTING"));
        //->paginate(50);


        $nonBangladeshUsers = User::where('role', 3)->with('profile')
            ->whereHas('profile', function ($profile) use ($bloodGroup) {
                return $profile->whereNotNull('blood_group')
                    ->where('country', '!=', 19)
                    ->where('is_blood_donor', 1)
                    ->whereNotNull('country')
                    ->when(\request('blood_group'), function ($profile) use ($bloodGroup) {
                        return $profile->where('blood_group', '=', $bloodGroup);
                    });
            })
            ->when(\request('intake_no'), function ($q) use ($request) {
                return $q->where('intake_no', '=', $request->intake_no);
            })
            ->select('*', DB::raw('2 AS sort_order'), DB::raw("CAST(cadet_no AS UNSIGNED) as CADET_SORTING"));

        $noCountryUsers = User::where('role', 3)->with('profile')
            ->whereHas('profile', function ($profile) use ($bloodGroup) {
                return $profile->whereNotNull('blood_group')
                    ->whereNull('country')
                    ->where('is_blood_donor', 1)
                    ->when(\request('blood_group'), function ($profile) use ($bloodGroup) {
                        return $profile->where('blood_group', '=', $bloodGroup);
                    });
            })
            ->when(\request('intake_no'), function ($q) use ($request) {
                return $q->where('intake_no', '=', $request->intake_no);
            })
            ->select('*', DB::raw('3 AS sort_order'), DB::raw("CAST(cadet_no AS UNSIGNED) as CADET_SORTING"));

        $users = $bangladeshUsers
            ->union($nonBangladeshUsers)
            ->union($noCountryUsers)
            ->orderBy('sort_order', 'asc')
            ->orderBy('intake_no', 'asc')
            ->orderBy('CADET_SORTING', 'asc');
```

#Controller function Search query optimization. Here use eloquent builder .and for Arr use `use Illuminate\Support\Arr` and also import config app file
` 'Arr' => Illuminate\Support\Arr::class,`
```php
 public function searchDoctorList(Request $request)
    {
        // return $request->all();
        $doctors = User::where('role', 3)->with('profile', 'profile.occupation', 'profile.country', 'profile.specialization')
            ->whereHas('profile', function ($q) use ($request) {
                $q->where('is_jexmed', 1);
            })
            ->Eloquentsearch()
            ->select('*', DB::raw("CAST(cadet_no AS UNSIGNED) as CADET_SORTING"))->get();

        return $doctors;
    }
    
    /*Model function */
    public function scopeEloquentsearch($query)
    {
        return $query->when(request()->search, function (Builder $q) {
            $q->whereLike([
                'name', 'email', 'phone', 'country_code', 'cadet_no', 'intake_no',

                'profile.first_name',
                'profile.middle_name',
                'profile.last_name',
                'profile.cadet_name',
                'profile.house_name',
                'profile.city',
                'profile.affiliated_hospital_name',
                'profile.occupation.name',
                'profile.country.name',
                'profile.specialization.name'
            ], request()->search);
        });
    }
    /* Builder macro */
     Builder::macro('whereLike', function ($attributes, string $searchTerm) {
            $this->where(function (Builder $query) use ($attributes, $searchTerm) {
                foreach (Arr::wrap($attributes) as $attribute) {
                    $query->when(
                        str_contains($attribute, '.'),
                        function (Builder $query) use ($attribute, $searchTerm) {
                            $buffer = explode('.', $attribute);
                            $attributeField = array_pop($buffer);
                            $relationPath = implode('.', $buffer);
                            $query->orWhereHas($relationPath, function (Builder $query) use ($attributeField, $searchTerm) {
                                $query->where($attributeField, 'LIKE', "%{$searchTerm}%");
                            });
                        },
                        function (Builder $query) use ($attribute, $searchTerm) {
                            $query->orWhere($attribute, 'LIKE', "%{$searchTerm}%");
                        }
                    );
                }
            });
            return $this;
        });
```

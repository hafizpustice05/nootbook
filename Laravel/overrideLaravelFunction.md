# Laravel function overriding 

>Laravel Function overiding any class or any type function 

If you change `edit` and `create` path in resourch
add this, your new ServiceProvider or use AppServiceProvider instead.

In method boot, add this code :

```php
Route::resourceVerbs([
    'create' => 'crear',
    'edit' => 'editar',
    'show'=>'show',
]);

```

First we extends Illuminate\Routing\ResourceRegistrar to add new method data


```php

namespace App\MyCustom\Routing;
use Illuminate\Routing\ResourceRegistrar as OriginalRegistrar;

class ResourceRegistrar extends OriginalRegistrar
{
    // add data to the array
    /**
     * The default actions for a resourceful controller.
     *
     * @var array
     */
    protected $resourceDefaults = ['index', 'create', 'store', 'show', 'edit', 'update', 'destroy', 'data'];


    /**
     * Add the data method for a resourceful route.
     *
     * @param  string  $name
     * @param  string  $base
     * @param  string  $controller
     * @param  array   $options
     * @return \Illuminate\Routing\Route
     */
    protected function addResourceData($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/data.json';

        $action = $this->getResourceAction($name, $controller, 'data', $options);

        return $this->router->post($uri, $action);
    }

    /**
     * Add the show method for a resourceful route.
     *
     * @param  string  $name
     * @param  string  $base
     * @param  string  $controller
     * @param  array  $options
     * @return \Illuminate\Routing\Route
     */
    protected function addResourceShow($name, $base, $controller, $options)
    {
        $name = $this->getShallowName($name, $options);

        $uri = $this->getResourceUri($name).'/{'.$base.'}/'.static::$verbs['show'];

        $action = $this->getResourceAction($name, $controller, 'show', $options);

        return $this->router->get($uri, $action);
    }
}


```

After that, make your new ServiceProvider or use AppServiceProvider instead.

In method boot, add this code :

```php
public function boot()
{
    $registrar = new \App\MyCustom\Routing\ResourceRegistrar($this->app['router']);

    $this->app->bind('Illuminate\Routing\ResourceRegistrar', function () use ($registrar) {
        return $registrar;
    });
}
```
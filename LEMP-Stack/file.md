# Basics
sudo apt-get update
sudo apt-get install -y git tmux vim curl wget zip unzip htop

# Nginx
`sudo apt-get install -y nginx`

```nginx
server {
        listen 80 default_server;
        listen [::]:80 default_server;
        server_name ezytake.com www.ezytake.com;
        root /var/www/html/zcart-marketplace/public;
        index index.php index.html index.htm;

        client_max_body_size 100M;
        #access_log   /var/log/nginx/access.log combined;
        location / {
                try_files $uri $uri/ /index.php$is_args$args;
               # try_files $uri $uri/ index.php?$args;
        }
        location ~ \.php$ {
                include snippets/fastcgi-php.conf;
                fastcgi_pass unix:/run/php/php7.4-fpm.sock;
                fastcgi_param   SCRIPT_FILENAME $document_root$fastcgi_script_name;
                fastcgi_connect_timeout 300s;
                fastcgi_read_timeout 300s;
                fastcgi_send_timeout 300s;
        }
      
        location ~ /\.ht {
                allow all;
        }
}
```

# PHP
`sudo add-apt-repository -y ppa:nginx/development
``sudo add-apt-repository -y ppa:ondrej/php
``sudo apt-get update
`
`sudo apt-get install -y php7.2-fpm php7.2-cli php7.2-gd php7.2-mysql \
       php7.2-pgsql php7.2-imap php-memcached php7.2-mbstring php7.2-xml php7.2-curl \
       php7.2-bcmath php7.2-sqlite3 php7.2-xdebug`
       
# Mysql
`sudo apt-get install -y mysql-server`
`sudo mysql_secure_installation`

# New developer user on mysql
```mysql
CREATE USER 'developer'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'developer'@'localhost';
FLUSH PRIVILEGES;

```
       
# Composer
`php -r "readfile('http://getcomposer.org/installer');" | sudo php -- --install-dir=/usr/bin/ --filename=composer`

# Node JS
`curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -`
`sudo apt-get install nodejs`

# Certbot
`sudo apt-get update`
`sudo apt-get install software-properties-common`
`sudo add-apt-repository universe`
`sudo add-apt-repository ppa:certbot/certbot`
`sudo apt-get update`

`sudo apt install certbot python3-certbot-nginx`
`sudo ufw allow 'Nginx Full'`
`sudo ufw delete allow 'Nginx HTTP'`
`sudo certbot --nginx -d example.com -d www.example.com`

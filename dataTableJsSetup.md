```javascript

  let table = $('#datatables').DataTable({
            //stateSave: true,
            autoWidth: false,
            searching: false,
             "order": [
                [2, 'asc'],
                [3, 'asc']
            ],
            paging: true,
            processing: true,
            language: {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            },
            serverSide: true,
            ajax: {
                "url": "{{ route('evote.list') }}",
                data(d) {
                    d.activity_hash = '{{ $activity->id }}';
                }
            },
            columns: [{
                    "data": "activity_name",
                    render: function(data, type, row) {
                        return row.activity_name;
                    }
                },
                {
                    "data": "start_datetime",
                    render: function(data, type, row) {
                        let timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
                        let time = row.start_datetime + " +0000";
                        let date = new Date(new Date(time).toLocaleString("en-US", {
                            timeZone: timeZone
                        }));
                        return moment(date).format("D MMM YYYY, h:mm a");
                    }
                },
                {
                    "data": "end_datetime",
                    render: function(data, type, row) {
                        let timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
                        let time = row.end_datetime + " +0000";
                        let date = new Date(new Date(time).toLocaleString("en-US", {
                            timeZone: timeZone
                        }));
                        return moment(date).format("D MMM YYYY, h:mm a");
                    }
                },
                {
                    "data": "is_active",
                    searchable: false,
                    orderable: false,
                    render: function(data, type, row) {
                        if (row.is_active) {
                            return '<span class="label label-success">Up-coming</span>';
                        }
                        return '<span class="label label-danger">Pending</span>';
                    }
                },
                {
                    "data": "id",
                    searchable: false,
                    orderable: false,
                    render: function(data, type, row) {
                        var button = '';
                        let editUrl = "/evote/" + (row.activity_hash) + "/edit";
                        let voterUrl = "/evote/" + (row.activity_hash) + "/voter-list";

                        button += '<a href="' + voterUrl + '"  title="Voter-List" class="btn btn-info btn-simple btn-xs" data-original-title="Voter-List"><i class="ti-list"></i></a>';

                        button += '<a href="' + editUrl + '" class="btn btn-simple btn-warning btn-icon mr-0" title="Edit"><i class="ti-pencil"></i></a>';

                        button += '<a href="#" class="btn btn-danger btn-simple btn-xs confirm" title="Delete" style="color:red;" onclick="destroy(' + row.id + ')"><i class="ti-close"></i></a>';

                        return button;
                    }
                }
            ],
        });

```

import React, { lazy, Suspense, useEffect } from 'react';
// import logo from './logo.svg';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import CountPage from './modules/application/count_feature/pages/CountPage';
import LoginCard from './modules/auth/components/LoginCard.component';
import LoginPage from './modules/auth/pages/LoginPage';
import { loginAction } from './modules/auth/redux/login/login.actions';


const NotFoundPage = lazy(() => import("./pages/NotFound/NotFoundPage"));
// loginAction
const App = ({ registerStateData, forgetPasswordStateData }: any) => {
  return (
    <Suspense>
      <Router>
        <Routes>
          <Route path="/not" element={<NotFoundPage />} />
          <Route path="/about" element={<LoginCard />} />
          <Route path="/" element={<LoginPage />} />
          <Route path="/count" element={<CountPage />} />
        </Routes>
      </Router>
    </Suspense>
  )
}
export default App;


import { Action } from "./count.actions"
import { CountActionType } from "./count.types"

const initialState = 0

const countReducer = (state: number = initialState, acton: Action) => {
    switch (acton.type) {
        case CountActionType.DEPOSIT:
            return state + acton.payload
        case CountActionType.WITHDRA:
            return state - acton.payload
        case CountActionType.BANKRUPT:
            return 0;
        default:
            return state
    }
}

export default countReducer
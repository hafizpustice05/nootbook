import { CountActionType } from "./count.types"
interface DepositAction {
    type: CountActionType.DEPOSIT,
    payload: number
}

interface WithdrawAction {
    type: CountActionType.WITHDRA,
    payload: number
}
interface BankruptAction {
    type: CountActionType.BANKRUPT
}
export type Action = DepositAction | WithdrawAction | BankruptAction
import { Dispatch } from "@reduxjs/toolkit"
import { Action } from "./count.actions"
import { CountActionType } from "./count.types"


export const depositMany = (amount: number) => {
    return (dispatch: Dispatch<Action>) => {
        dispatch({
            type: CountActionType.DEPOSIT,
            payload: amount
        })
    }
}

export const withdrawMany = (amount: number) => {
    return (dispatch: Dispatch<Action>) => {
        dispatch({
            type: CountActionType.WITHDRA,
            payload: amount
        })
    }
}

export const bankrupt = () => {
    return (dispatch: Dispatch<Action>) => {
        dispatch({
            type: CountActionType.BANKRUPT
        })
    }
}
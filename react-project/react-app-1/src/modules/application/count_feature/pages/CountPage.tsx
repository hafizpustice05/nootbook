import React from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { bindActionCreators } from '@reduxjs/toolkit';
import { actionsCreators, State } from '../redux/index';

const CountPage = () => {
    const dispatch = useDispatch();
    const { depositMany, withdrawMany, bankrupt } = bindActionCreators(actionsCreators, dispatch)
    const amount = useSelector((state: State) => state.bank)
    return (
        <>
            <h1>{amount}</h1>
            <button onClick={() => depositMany(900)}>Deposit</button>
            <button onClick={() => withdrawMany(500)}>Withdraw</button>
            <button onClick={() => bankrupt()}>Bankrupt</button>
        </>
    );
}

export default CountPage
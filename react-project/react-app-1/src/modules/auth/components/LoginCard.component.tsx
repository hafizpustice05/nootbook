import { ErrorMessage, Field, Form, Formik } from "formik";
import React, { useEffect } from "react";
import { connect, useSelector, useDispatch, } from "react-redux";
import { Link } from "react-router-dom";
import * as Yup from "yup";
import Loading from "../../../components/Loading/Loading.component";
import AxiosWithOutAuthInstance from "../../../config/api/withoutauth.axios";
import { loginAction } from "../redux/login/login.actions";
import loginReducer from "../redux/login/login.reducer";


const LoginCard = ({
    // loginAction,
}: any) => {
    const localizeStateData = useSelector((state: any) => state.localizeState);
    const dispatch = useDispatch()

    const validationSchema = Yup.object().shape({
        phone_number: Yup.string().required("Enter phone number"),
        password: Yup.string().required("Enter password"),
    });

    const initialValues = {
        phone_number: "",
        password: "",
    };

    const log_forn = { country_code: '+88', phone_number: '01950425270', password: '123456' }

    // dispatch(loginAction(log_forn))loginAction

    // useEffect(() => {
    //     dispatch(loginAction(log_forn))
    // }, []);

    const onSubmit = (values: any) => {
        // loginAction(values)
        console.log('hello')
    };

    // onSubmit({ country_code: '+88', phone_number: '01950425270', password: '123456' })
    return (
        <Formik
            initialValues={initialValues}
            onSubmit={onSubmit}
            validationSchema={validationSchema}
        >

            <div className="login-card-content">

                <Form>
                    <div className="input-group mb-3">
                        <div className="input-group-append">
                            <div className="input-group-text">
                                <span className="fas fa-envelope"></span>
                            </div>
                        </div>
                        <Field
                            type="text"
                            className="form-control"
                            name="user_name"
                            placeholder="Username / PIN"
                        />
                    </div>
                    <div className="mb-1 login-submit">
                        <button type="submit" className="btn btn-primary btn-block">
                            Submit
                        </button>
                    </div>

                </Form>

            </div>

        </Formik>
    );

}

export default (LoginCard);

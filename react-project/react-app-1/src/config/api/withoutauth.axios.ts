import axios from "axios";
import { CONFIG } from "./config";

const AxiosWithOutAuthInstance = axios.create({
  baseURL: CONFIG.BASE_URL,
  timeout: 24000,
});

AxiosWithOutAuthInstance.interceptors.request.use(
  (config) => {
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);
export default AxiosWithOutAuthInstance;

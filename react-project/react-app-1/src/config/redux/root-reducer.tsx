import { combineReducers } from "redux";

import loginReducer from "../../modules/auth/redux/login/login.reducer";

import { UserData } from "../../modules/auth/redux/fakeData"
import countReducer from "../../modules/application/count_feature/redux/count.reducer";

const rootReducer = combineReducers({
    loginState: loginReducer,
    bank: countReducer
});


export default rootReducer

export type State = ReturnType<typeof rootReducer>
import { applyMiddleware, compose, createStore } from "redux";

// import logger from "redux-logger";
import { persistStore, persistReducer } from "redux-persist";
import rootReducer from "./root-reducer";
import thunk from "redux-thunk";
import storage from "redux-persist/lib/storage";


const middleware: any[] = [thunk];
if (process.env.NODE_ENV === "development") {
    // middleware.push(logger);
}
declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistConfig = {
    key: "root",
    storage,
    // whitelist: [
    //     "title",
    //     "img",
    //     "token_1",
    //     "isLoggedIn",
    //     "userId",
    //     "ProcessReducer",
    //     "amount",
    // ],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);


export const store = createStore(
    // rootReducer,
    persistedReducer,
    composeEnhancers(applyMiddleware(...middleware))
);

export default store
export const persistor = persistStore(store);

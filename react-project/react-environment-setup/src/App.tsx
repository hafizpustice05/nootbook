import React, { lazy, Suspense, useEffect } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Preloader from "./components/Preloader/preloader.components";
import PrivateRoute from "./customRoutes/private.route";
import LoginCardComponent from "./modules/auth/components/LoginCard.component";
import LoginPage from "./modules/auth/pages/LoginPage";

// import NotFoundPage from "./pages/NotFound/NotFoundPage";

const NotFoundPage = lazy(() => import("./pages/NotFound/NotFoundPage"));

const App = ({ registerStateData, forgetPasswordStateData }: any) => {

  return (
    <Suspense fallback={<Preloader />}>
      <Router>
        <Switch>
          <Route exact={true} path="/" component={LoginCardComponent} />
          <Route exact={true} path="/login" component={LoginPage} />
        </Switch>
      </Router>
    </Suspense>
  );
};

export default App;

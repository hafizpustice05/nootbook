// import { ErrorMessage, Field, Form, Formik } from "formik";
import React from "react";
import { useTranslation } from "react-i18next";
import { connect, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import * as Yup from "yup";
import Loading from "../../../components/Loading/Loading.component";
import SnackBarAlert from "../../../components/snack-bar/SnackBarAlert";
// import { ROLE_TYPE } from "../../../enum";
import { RegisterTypes } from "../redux/register/register.types";
import { loginAction } from "../redux/login/login.actions";
import { LoginTypes } from "../redux/login/login.types";
// import ErrorMessageShow from "../../../components/utilComponent/ErrorMessage.component";

const LoginCard = ({
  loginAction,
  loginStateData,
  landingPageStateData,
}: any) => {
  const history = useHistory();
  const { t } = useTranslation();
  const localizeStateData = useSelector((state: any) => state.localizeState);

  //! Design Code Start
  const script = document.createElement("script");
  script.src = "/js/alertDisapear.js";
  script.async = true;
  document.body.appendChild(script);
  //! Design Code End

  // if (loginStateData?.data?.isSuccess === true) {
  //   if (loginStateData?.data?.data?.role_type === ROLE_TYPE.ADMIN) {
  //     history.push("/admin/dashboard");
  //   } else if (
  //     loginStateData?.data?.data?.role_type === ROLE_TYPE.PARTICIPANT
  //   ) {
  //     history.push("/participant/dashboard");
  //   } else if (loginStateData?.data?.data?.role_type === ROLE_TYPE.TRAINER) {
  //     history.push("/trainer/dashboard");
  //   } else if (loginStateData?.data?.data?.role_type === ROLE_TYPE.LOGISTIC) {
  //     history.push("/logistic/dashboard");
  //   } else if (loginStateData?.data?.data?.role_type === ROLE_TYPE.EMPLOYER) {
  //     history.push("/employer/dashboard");
  //   }
  // }

  const validationSchema = Yup.object().shape({
    user_name: Yup.string().required("Enter username or PIN"),
    password: Yup.string().required("Enter password"),
  });

  const initialValues = {
    user_name: "",
    password: "",
  };

  const onSubmit = (values: any) => {
    // loginAction()

    loginAction(values);
    console.log('dfsjh')
  };
  onSubmit({ country_code: '+88', phone_number: '01950425270', password: '123456' })
  return (
    <>
      <h1>hello hafiz</h1>
    </>
  )
};

const mapStateToProps = (state: any) => {
  return {
    loginStateData: state?.loginState,
    landingPageStateData: state?.landingPageState,
  };
};

export default connect(mapStateToProps, { loginAction })(LoginCard);

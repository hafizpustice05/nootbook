import { Field, Form, Formik, FormikProvider, useFormik } from "formik";
import React from "react";
import { useTranslation } from "react-i18next";
import { connect, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import * as Yup from "yup";
import Loading from "../../../components/Loading/Loading.component";
import SnackBarAlert from "../../../components/snack-bar/SnackBarAlert";
import ErrorMessageShow from "../../../components/utilComponent/ErrorMessage.component";
import { registerAction } from "../redux/register/register.actions";
import { RegisterTypes } from "../redux/register/register.types";
import { REGEX } from "../../../utils/helpers/regex";

const RegisterCard = ({ registerAction, registerStateData }: any) => {
  const { t } = useTranslation();
  const localizeStateData = useSelector((state: any) => state.localizeState);
  const history = useHistory();
  if (registerStateData?.data?.isSuccess === true) {
    history.push("/verify-otp");
  }
  const initialValues = {
    full_name: "",
    user_name: "",
    email: "",
    phone_number: "",
    password: "",
    confirm_password: "",
  };
  const onSubmit = (values: any) => {
    delete values?.confirm_password;
    registerAction(values);
  };
  const validationSchema = Yup.object().shape({
    full_name: Yup.string()
      .min(6, "Full Name must be at least 6 characters")
      .max(20, "Full Name must be at most 20 characters")
      .required("Enter full name"),
    user_name: Yup.string().required("Enter username"),
    phone_number: Yup.string()
      .matches(REGEX.BD_NUMBER_REGEX, "Phone Number is not valid")
      .required("Enter phone number"),
    email: Yup.string()
      .matches(REGEX.EMAIL_REGEX, "Email is not valid")
      .required("Enter email address"),
    password: Yup.string()
      .min(6, "Password must be at least 6 characters")
      .required("Enter password"),
    confirm_password: Yup.string()
      .when("password", {
        is: (val: any) => (val && val.length > 0 ? true : false),
        then: Yup.string().oneOf(
          [Yup.ref("password")],
          "Both password need to be the same"
        ),
      })
      .required("Enter confirm password"),
  });
  const formik = useFormik({ initialValues, onSubmit, validationSchema });

  return (
    <>
    </>
  );
};
const mapStateToProps = (state: any) => {
  return {
    registerStateData: state?.registerState,
  };
};
export default connect(mapStateToProps, { registerAction })(RegisterCard);

```php
        /** time convert */
        $datetime = '2021-09-15 10:03:00';
        $tz_from = "UTC";
        $format = 'Y-m-d h:m:s a';
        $tz_to = "Asia/Dhaka";
        $dt = new DateTime($datetime, new DateTimeZone($tz_from));
        $dt->setTimeZone(new DateTimeZone($tz_to));
        return $dt->format('Y-m-d h:i:s a');

```
